# GitLab Builder Images

This project builds and publishes Docker images that are used by
the CI pipeline of [omnibus-gitlab](https://gitlab.com/gitlab-org/omnibus-gitlab) to build official GitLab packages. It
contains Dockerfiles for all the official OSs for which packages are provided,
with a few additional Docker images used by various other housekeeping CI jobs in
`omnibus-gitlab`.

Push mirroring is configured from
[GitLab.com repository](https://gitlab.com/gitlab-org/gitlab-omnibus-builder)
to the [dev.gitlab.org mirror](https://dev.gitlab.org/cookbooks/gitlab-omnibus-builder)

## Versioning strategy

This project uses a custom versioning strategy that fits the usage and update
pattern of these builder images. It follows the `X.Y.Z` scheme, but when each
component is bumped depends on the type of update happening. This is described
below:

1. X component is bumped when we refactor the build process or strategy, or we
   change any of the base images used for the builder images. We consider these
   as major changes that require a new major version cut.

1. Y component is bumped in following scenarios
    1. When the versions of any of the components in the images gets updated to
       a newer version.
    1. When a new OS is added to the list
    1. Other minor changes to the Dockerfiles, or build process of any
       components.

1. Z component is reserved for bringing in changes to older versions of builder
   images that are required by GitLab backport releases. For example, when a
   security backport for GitLab to a version that used an older version of these
   builder images (say, `1.0.0`) require some changes to the builder images
   themselves, we branch off `1.0.0`, make the changes and tag `1.0.1` off that.

## Cutting new releases

To cut a new release:

```
git tag -a VERSION_NUMBER -m "Reason for update"
git push --follow-tags
```

This will trigger a continuous integration job that creates the images, tags
them with `VERSION_NUMBER`, and makes them available in the container
registry.

## License and Authors

Contents of this project are distributed under the MIT license, see LICENSE.
