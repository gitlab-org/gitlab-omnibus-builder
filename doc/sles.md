# SuSE Enterprise Linux (SLES)

## Overview

GitLab creates packages for SLES versions 12 SP2, 12 SP5, 15 SP2, and 15 SP6.

To create packages for versions before SLES 15 SP3, we make use of SLES-based containers
that are either:

- On a dedicated SLES host, which are isolated and prone to lack of care.
- Made from a SLES host and pointed to a
  [Repository Mirroring Tool (RMT)][RMT] server as a providing mirror.

Due to licensing concerns, GitLab does not provide the associated SLES-based
containers on the public registry for GitLab Omnibus Builder.

For packages later than SLES 15 SP3, we publish the OpenSUSE Leap packages
to the SLES repositories because they are [binary compatible](https://www.suse.com/c/introducing-suse-linux-enterprise-15-sp3/).

## Containers

The containers are provided on GitLab's Dev instance only. They are created
during the CI process, and will only run on the Dev instance. They use a [Dockerfile][]
that includes all repository additions, package installation, and repository
removals in a single step, so that we maintain the privacy of the `RMT_HOST`.
`RMT_HOST` is provided as a part of the CI secrets in the project.

## Accessing base images

Base images are pulled from SUSE's Docker registry and pushed into GitLab's
internal registry. Below are the steps to follow to push the relevant SLES
base image into GitLab's internal registry.

Note that pushing the image to the GitLab internal registry requires maintainer
access on the `dev.gitlab.org/cookbooks/gitlab-omnibus-builder` project.

1. Log into the RMT host.
1. `docker pull registry.suse.com/suse/slesXspY`, where `X` is the major version
     and `Y` is the service pack version.
1. `docker tag registry.suse.com/suse/slesXspY dev.gitlab.org:5005/cookbooks/gitlab-omnibus-builder/suse/slesXspX`
1. `docker login dev.gitlab.org:5005`, using your GitLab username and personal access
     token to log in.
1. `docker push dev.gitlab.org:5005/cookbooks/gitlab-omnibus-builder/suse/slesXspX`

After these steps, the base image will be available in the registry and can be used
in the `FROM` statement for the relevant SLES Dockerfiles.

## About the RMT

Sensitive information regarding the RMT server can be found in the `SLES RMT` note
of the `Build` vault.

The RMT server acts as a local package mirror for the containers to be able to
install packages that are up to date for the versions of SLES currently being
supported. The estimated storage requirements are 10 GB per version.

Management of the available mirrored repositories is handled via the `rmt-cli` command.
Documentation about how this is done is provided in the [mirroring documentation](https://documentation.suse.com/sles/15-SP2/html/SLES-all/cha-rmt-mirroring.html#sec-rmt-mirroring-enable-disable-repository).

Because this is a licensed instance, we can not expose it publicly.

[RMT]: https://documentation.suse.com/sles/15-SP2/html/SLES-all/book-rmt.html
[Dockerfile]: https://docs.docker.com/engine/reference/builder/
