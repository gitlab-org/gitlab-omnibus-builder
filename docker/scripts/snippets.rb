# frozen_string_literal: true

class Snippets
  DOCKER_DIR = File.expand_path(File.join(File.dirname(__FILE__), '../'))
  SNIPPETS_DIR = File.join(DOCKER_DIR, 'snippets')
  VERSIONS_FILE = File.join(DOCKER_DIR, 'VERSIONS')

  def initialize(include: nil, exclude: nil, platform: nil, fips: false)
    default_snippets = %w[versions cmake git go rust ruby node yarn cache gitconfig tmpclean]
    @include_snippets = include || default_snippets
    @exclude_snippets = exclude || []
    @platform = "_#{platform}" if platform
    @fips = "_fips" if fips
    @content = []
  end

  def valid_snippets
    @valid_snippets ||= @include_snippets - @exclude_snippets
  end

  def populate
    if valid_snippets.include?('versions')
      @content << versions
      @content << "\n"
      @valid_snippets.delete('versions')
    end
    @content << snippets.join("\n")
    @content.join('')
  end

  def versions
    File.readlines(VERSIONS_FILE).map do |component|
      "ENV #{component}"
    end
  end

  def snippets
    valid_snippets.map do |component|
      filename = File.join(SNIPPETS_DIR, "#{component}#{@platform}#{@fips}")
      filename = filename.delete_suffix(@fips) if @fips && !File.exist?(filename)
      filename = filename.delete_suffix(@platform) unless File.exist?(filename)

      File.read(filename)
    end
  end
end
